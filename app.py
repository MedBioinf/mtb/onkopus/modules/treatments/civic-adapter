from civic_adapter.process_request import process_variant_request, process_gene_request, process_nearby_biomarker_request
from civic_adapter.db_requests import get_molecular_profiles_for_clinical_evidence, get_gene_summary
from flask import request
from flask_cors import CORS
import conf.read_config as config

DEBUG = True

from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)
app.config.from_object(__name__)

SERVICE_NAME='civic-adapter'
VERSION='v1'

CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = '/civic-adapter/v1/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "CIViC Adapter"
    },
)

# definitions
SITE = {
        'logo': 'FLASK-VUE',
        'version': '0.0.1'
}


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/GenomicClinicalEvidence', methods=['GET'])
def get_interpretations(genome_version=None):
    q= request.args.get("q")
    genomepos = request.args.get("genompos")

    if genomepos is not None:
        genomepos = genomepos.split(",")

    genome_version = genome_version
    pos_as_keys = request.args.get("key")

    res = process_variant_request(q=q,genome_version=genome_version,genomepos=genomepos,pos_as_keys=pos_as_keys)
    #res = get_molecular_profiles_for_clinical_evidence(res)

    return res


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/GeneClinicalEvidence', methods=['GET'])
def get_gene_interpretations(genome_version=None):
    q= request.args.get("q")
    genes = request.args.get("gene")

    if genes is not None:
        genes = genes.split(",")

    genome_version = genome_version
    pos_as_keys = request.args.get("key")

    res = process_gene_request(q=q,genome_version=genome_version,genomepos=genes,pos_as_keys=pos_as_keys)
    #res = get_molecular_profiles_for_clinical_evidence(res)

    return res


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/GenomicNearbyClinicalEvidence', methods=['GET'])
def get_nearby_interpretations(genome_version=None):
    q = request.args.get("q")
    genes = request.args.get("gene")

    if genes is not None:
        genes = genes.split(",")

    genome_version = genome_version
    pos_as_keys = request.args.get("key")

    res = process_nearby_biomarker_request(q=q,genome_version=genome_version,genomepos=genes,pos_as_keys=pos_as_keys)
    #res = get_molecular_profiles_for_clinical_evidence(res)

    return res

@app.route(f'/{SERVICE_NAME}/{VERSION}/info')
def info_endpoint():
    info_data = {}
    info_data["title"] = "Clinical Interpretation of Variants in Cancer (CIViC) Service"
    info_data["database_version"] = "01-May-2023"
    info_data["databases_source"] = "https://civicdb.org/downloads/01-Apr-2023/01-May-2023-ClinicalEvidenceSummaries.tsv"
    info_data["database_website"] = "https://civicdb.org/releases/main"
    info_data["database_clinical_evidence_size"] = "3248694KB, 3,1MB"

    return info_data


if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=10106)
