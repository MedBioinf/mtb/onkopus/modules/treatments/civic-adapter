# CIViC-Adapter

Adapter for retrieving clinical significance data from the CIViC database and API. 

## Getting started

### Data Processing

The CIViC adapter extracts a set of features from CIViC and stores it in the normalized feature section ("civic_feature_norm"). 
The adapter performs the following steps to normalize features: 

- Mapping of feature keys
- Mapping of feature values
- Mapping of evidence levels to Onkopus evidence level score system

#### Mapping of feature keys

#### Mapping of feature values




