import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

match_types = ["exact_match","same_position","same_position_any_mutation", "any_mutation_in_gene", "nearby_mutations"]

if "MODULE_SERVER" in os.environ:
    __MODULE_SERVER__ = os.getenv("MODULE_SERVER")
else:
    __MODULE_SERVER__ = config['DEFAULT']['MODULE_SERVER']

if "CIVIC_DATABASE" in os.environ:
    __CIVIC_DATABASE__ = os.getenv("CIVIC_DATABASE")
else:
    __CIVIC_DATABASE__ = config['CIVIC']['CIVIC_DATABASE']

if "CIVIC_DB_PORT" in os.environ:
    __CIVIC_DB_PORT__ = os.getenv("CIVIC_DB_PORT")
else:
    __CIVIC_DB_PORT__ = config['CIVIC']['CIVIC_DB_PORT']

if "FEATURES" in os.environ:
    __FEATURES__ = os.getenv("FEATURES").split(",")
else:
    __FEATURES__ = config['CIVIC']['FEATURES'].split(",")

if "FEATURES_MOLPROFILES" in os.environ:
    __FEATURES_MOLPROFILES__ = os.getenv("FEATURES_MOLPROFILES").split(",")
else:
    __FEATURES_MOLPROFILES__ = config['CIVIC']['FEATURES_MOLPROFILES'].split(",")

if "FEATURES_GENESUM" in os.environ:
    __FEATURES_GENESUM__ = os.getenv("FEATURES_GENESUM").split(",")
else:
    __FEATURES_GENESUM__ = config['CIVIC']['FEATURES_GENESUM'].split(",")
