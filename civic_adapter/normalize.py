import conf.read_config as conf_reader
import copy

agg_keys = ['gene', 'variant', 'disease', 'drugs', 'evidence_level', 'evidence_type', 'citation_url', 'citation_id','source','response','evidence_statement']
civic_extract_keys = [ 'gene', 'variant', 'disease', 'drugs', 'evidence_level', 'evidence_type','citation_id', 'citation_id','civic','clinical_significance','evidence_statement' ]

# evidence level mapping
civic_levels = [ "A", "B", "C", "D", "E" ]
onkopus_levels = [ "1", "2", "3", "4", "5" ]


def normalize_response_type(response_type):

    if response_type == 'Sensitivity/Response':
        response_type_new = "Sensitive"
    else:
        response_type_new = response_type

    return response_type_new


def map_evidence_levels(variant_data):

    for var in variant_data.keys():
        if 'civic_features_norm' in variant_data[var]:
            for match_type in conf_reader.match_types:
                if match_type in variant_data[var]['civic_features_norm']:
                    for result in variant_data[var]['civic_features_norm'][match_type]:
                        if result["evidence_level"] in civic_levels:
                            index = civic_levels.index(result["evidence_level"])
                            result["evidence_level_onkopus"] = onkopus_levels[index]
                else:
                    variant_data[var]['civic_features_norm'][match_type] = []

    return variant_data


def normalize_civic_feature_values(variant_data):
    for var in variant_data.keys():
        if 'civic_features_norm' in variant_data[var]:
            for match_type in conf_reader.match_types:
                if match_type in variant_data[var]["civic"]:
                    for i, _ in enumerate(variant_data[var]['civic_features_norm'][match_type]):

                        # Citation URL
                        variant_data[var]['civic_features_norm'][match_type][i]['citation_url'] = \
                            conf_reader.config["SOURCE_SPECIFIC"]["PMID_BASE_URL"] + variant_data[var]['civic_features_norm'][match_type][i]['citation_url']

                        # Response Type
                        variant_data[var]['civic_features_norm'][match_type][i]['response'] = \
                            normalize_response_type(variant_data[var]['civic_features_norm'][match_type][i]['response'])

    return variant_data


def normalize_civic_feature_keys_section(var, variant_data, section):
    """


    :param var:
    :param variant_data:
    :param section:
    :return:
    """
    civic_extracted_data = []
    for i, result in enumerate(variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section]):

        if variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["therapy_interaction_type"] == "Substitutes":
            for drug in variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["therapies"].split(","):
                dc = {}
                dc["biomarker"] = variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i][
                    "molecular_profile"]
                # civic_extracted_data[i]["gene"] = gene
                # civic_extracted_data[i]["variant"] = variant

                dc["disease"] = variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i][
                    "disease"]

                dc["drugs"] = []
                #for drug_el in variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i][
                #    "therapies"].split(","):
                drug_dc = {}
                drug_dc["drug_name"] = drug
                dc["drugs"].append(drug_dc)

                dc["evidence_level"] = \
                    variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["evidence_level"]

                dc["evidence_type"] = \
                    variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["evidence_type"]

                dc["citation_url"] = \
                    variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["citation_id"]

                dc["citation_id"] = \
                    variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["citation_id"]

                dc["evidence_statement"] = \
                    variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["evidence_statement"]

                dc = extract_significance(dc,variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i])

                # Molecular profile link
                dc['source_link'] = variant_data[var]['civic'][section][i]['molecular_profile_civic_url']

                dc['source'] = 'civic'
                dc["match_type"] = section
                civic_extracted_data.append(dc)
        else:
            dc={}
            # print("properties: ",variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][i].keys())
            # print(variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][i]["molecular_profile"].split(" "))

            dc["biomarker"] = variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i][
                "molecular_profile"]
            # civic_extracted_data[i]["gene"] = gene
            # civic_extracted_data[i]["variant"] = variant

            dc["disease"] = variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i][
                "disease"]

            dc["drugs"] = []
            for drug in variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["therapies"].split(","):
                drug_dc = {}
                drug_dc["drug_name"] = drug
                dc["drugs"].append(drug_dc)


            dc["evidence_level"] = \
                variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["evidence_level"]

            dc["evidence_type"] = \
                variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["evidence_type"]

            dc["citation_url"] = \
                variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["citation_id"]

            dc["citation_id"] = \
                variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["citation_id"]

            dc["evidence_statement"] = \
                variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i]["evidence_statement"]

            dc = extract_significance(dc,variant_data[var][conf_reader.config['MODULES']['CIVIC_PREFIX']][section][i])

            # Molecular profile link
            dc['source_link'] = variant_data[var]['civic'][section][i]['molecular_profile_civic_url']

            dc['source'] = 'civic'
            dc["match_type"] = section
            civic_extracted_data.append(dc)
    return civic_extracted_data


def normalize_civic_feature_keys(variant_data):
    """
    Selects a set of features that are present in all evidence databases from the CIViC API features and assigns the same names to all feature keys

    :param variant_data:
    :return:
    """
    variant_data_new = copy.deepcopy(variant_data)
    for var in variant_data.keys():
        if 'civic' in variant_data[var]:
            #print(var)
            variant_data_new[var]['civic_features_norm'] = {}
            for match_type in conf_reader.match_types:
                if match_type in variant_data[var]["civic"]:
                    civic_extracted_data = normalize_civic_feature_keys_section(var, variant_data, match_type)
                else:
                    civic_extracted_data = []

                variant_data_new[var]['civic_features_norm'][match_type] = copy.deepcopy(civic_extracted_data)
        else:
            print("no civic section: ",var,":",variant_data[var])

    return variant_data_new


def extract_significance(civic_extracted_data, var_arr):
    """
    Extracts significance data from the CIViC database

    :param civic_extracted_data:
    :param i:
    :param var_arr:
    :return:
    """

    if var_arr["significance"] == "Resistance":
        civic_extracted_data["response"] = "Resistant"
    elif var_arr["significance"] == "Sensitivity/Response":
        civic_extracted_data["response"] = "Sensitive"
    elif var_arr["significance"] == "Positive":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Negative":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Adverse Response":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Better Outcome":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Dominant Negative":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Gain of Function":
        civic_extracted_data["functional_effect"] = "Gain of Function"
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Likely Pathogenic":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Loss of Function":
        civic_extracted_data["functional_effect"] = "Loss of Function"
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "N/A":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Neomorphic":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Oncogenicity":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Pathogenic":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Poor Outcome":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Predisposition":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Reduced Sensitivity":
        civic_extracted_data["response"] = ""
    elif var_arr["significance"] == "Uncertain Significance":
        civic_extracted_data["response"] = ""

    return civic_extracted_data
