
def merge_dictionaries(a, b, path=None):
    """
    Merges dictionary b into dictionary a without data loss. C

    :param a:
    :param b:
    :param path:
    :return:
    """
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dictionaries(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass # same leaf value
            else:
                if isinstance(a[key], list) and isinstance(b[key], list):
                    a[key] = a[key] + b[key] # concatenate lists
                else:
                    raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a
