import re

def parse_variant_exchange(variant_exchange):
    rsregex = "([A-Z]+)([0-9]+)([A-Z\\?\\*=.]+)"
    if re.compile(rsregex).match(variant_exchange):
        p = re.compile(rsregex).match(variant_exchange).groups()
        aaref = p[0]
        pos = p[1]
        aaalt = p[2]
        return aaref, pos, aaalt
    else:
        return None, None, None
