import copy
import pymysql
import conf.read_config as conf_reader
from civic_adapter import normalize
import civic_adapter.tools.parse_genomic_data
from civic_adapter.tools.data_structures import merge_dictionaries

user="root"
pw="civicdb-pw"


def search_for_any_mutation_in_same_gene(res_data, variants):
    """
    Searches the CIViC database for clinical evidence for gene mutations

    :param res_data:
    :param variants:
    :return:
    """
    for i, var in enumerate(variants):
        is_var = False
        if ":" in var:
            elements = var.split(":")
            gene = elements[0]
            variant = elements[1]
            variant_str = gene + " " + variant
            is_var = True
            identifier = var
            aaref, pos, aaalt = civic_adapter.tools.parse_genomic_data.parse_variant_exchange(variant)
        else:
            gene = var
            identifier = var

        # Get clinical evidence data
        cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
        cursor = cnx.cursor()

        mysql_q = ("SELECT " + ",".join(
            conf_reader.__FEATURES__) + " from clinical_evidence where (molecular_profile='" + gene + " Mutation');")
        print(mysql_q)
        cursor.execute(mysql_q)

        res_data[var] = {}
        res_data[var]["any_mutation_in_gene"] = []
        result = cursor.fetchall()

        features = copy.deepcopy(conf_reader.__FEATURES__)
        json_obj = {}
        for ((result)) in enumerate(result):
            json_obj={}
            for j, feature in enumerate(features):
                json_obj[feature] = result[1][j]
            res_data[var]["any_mutation_in_gene"].append(copy.deepcopy(json_obj))

        cursor.close()
        cnx.close()

    return res_data


def search_for_same_position_any_mutations(res_data, variants, genomepos=None,pos_as_keys=None):
    """
    Searches for CIViC data with mutations at the same position that do have other amino acid exchanges than the targeted variant but refer not to any mutation,
    i.e. finds [NRAS Q61K, NRAS Q61R, NRAS Q61H] for target NRAS:Q61L, but not [NRAS:Q61]

    :param res_data:
    :param variants:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    for i, var in enumerate(variants):
        elements = var.split(":")
        gene = elements[0]
        variant = elements[1]
        variant_str = gene + " " + variant

        aaref, pos, aaalt = civic_adapter.tools.parse_genomic_data.parse_variant_exchange(variant)

        if (gene is not None) and (aaref is not None) and (pos is not None):

            # Get clinical evidence data
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
            cursor = cnx.cursor()

            mysql_q = ("SELECT " + ",".join(
                conf_reader.__FEATURES__) + " from clinical_evidence where (molecular_profile='" + gene + " " + aaref + pos + "');")
            print(mysql_q)
            cursor.execute(mysql_q)

            res_data[gene + ":" + variant] = {}
            res_data[gene + ":" + variant]["same_position_any_mutation"] = []
            result = cursor.fetchall()

            features = copy.deepcopy(conf_reader.__FEATURES__)
            json_obj = {}
            for ((result)) in enumerate(result):
                json_obj={}
                for j, feature in enumerate(features):
                    json_obj[feature] = result[1][j]
                res_data[gene + ":" + variant]["same_position_any_mutation"].append(copy.deepcopy(json_obj))

            cursor.close()
            cnx.close()

    return res_data


def search_for_same_position_matches(res_data, variants, genomepos=None,pos_as_keys=None):
    """
    Searches for CIViC data with mutations at the same position that do have other amino acid exchanges than the targeted variant but refer not to any mutation,
    i.e. finds [NRAS Q61K, NRAS Q61R, NRAS Q61H] for target NRAS:Q61L, but not [NRAS:Q61]

    :param res_data:
    :param variants:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    for i, var in enumerate(variants):
        elements = var.split(":")
        gene = elements[0]
        variant = elements[1]
        variant_str = gene + " " + variant

        aaref, pos, aaalt = civic_adapter.tools.parse_genomic_data.parse_variant_exchange(variant)

        if (aaref is not None) and (pos is not None) and (aaalt is not None):
            # Get clinical evidence data
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
            cursor = cnx.cursor()

            mysql_q = ("SELECT " + ",".join(
                conf_reader.__FEATURES__) + " from clinical_evidence where (molecular_profile LIKE '" + gene + " " + aaref + pos + \
                       "%' and molecular_profile REGEXP '^" + gene + " " + aaref + pos + "[A-Za-z]+$') " \
                "and (molecular_profile!='" + variant_str + "') and (molecular_profile!='" + gene + " " + aaref + pos + "');")
            print(mysql_q)
            cursor.execute(mysql_q)

            res_data[gene + ":" + variant] = {}
            res_data[gene + ":" + variant]["same_position"] = []

            result = cursor.fetchall()

            features = copy.deepcopy(conf_reader.__FEATURES__)
            json_obj = {}
            for ((result)) in enumerate(result):
                #print("res ",result[0])
                json_obj={}
                for j, feature in enumerate(features):
                    #print("result ",i,":",feature,":",result[0],":",result[1][j])
                    json_obj[feature] = result[1][j]

                res_data[gene + ":" + variant]["same_position"].append(copy.deepcopy(json_obj))

            cursor.close()
            cnx.close()
        else:
            print("Error (same position matches): Could not parse ", variant)

    return res_data


def search_for_exact_variant_matches(res_data, variants, genomepos=None,pos_as_keys=None):
    """
    Searches for CIViC data with the same molecular profile, i.e. the same gene and the same variant

    :param res_data:
    :param variants:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    for i, var in enumerate(variants):
        elements = var.split(":")
        gene = elements[0]
        variant = elements[1]
        variant_str = gene + " " + variant

        # Get clinical evidence data
        if (variant_str is not None) and (variant_str!="") and (variant_str!=" "):
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
            cursor = cnx.cursor()

            mysql_q = ("SELECT " + ",".join(
                conf_reader.__FEATURES__) + " from clinical_evidence where molecular_profile='" + variant_str + "';")
            print(mysql_q)
            cursor.execute(mysql_q)

            res_data[gene + ":" + variant] = {}
            res_data[gene + ":" + variant]["exact_match"] = []
            result = cursor.fetchall()

            features = copy.deepcopy(conf_reader.__FEATURES__)
            json_obj = {}
            for ((result)) in enumerate(result):
                #print("res ",result[0])
                json_obj={}
                for j, feature in enumerate(features):
                    #print("result ",i,":",feature,":",result[0],":",result[1][j])
                    json_obj[feature] = result[1][j]
                res_data[gene + ":" + variant]["exact_match"].append(copy.deepcopy(json_obj))

            cursor.close()
            cnx.close()

    return res_data


def get_molecular_profiles_for_clinical_evidence(bframe):
    """
    Returns the molecular profiles for each CIViC treatment result in a biomarker frame

    :param bframe:
    :return:
    """

    for i, var in enumerate(bframe):
        bframe[var]["civic"]["molecular_profiles"] = {}
        for match_type in conf_reader.match_types:
            if match_type in bframe[var]["civic"]:
                for j,_ in enumerate(bframe[var]["civic"][match_type]):
                    if "molecular_profile_id" in bframe[var]["civic"][match_type][j]:
                        molID = bframe[var]["civic"][match_type][j]["molecular_profile_id"]
                        mol_data = get_molecular_profile(molID)
                        #mol_biomarker = mol_data["molecular_profile"]
                        bframe[var]["civic"]["molecular_profiles"][molID] = mol_data
                    else:
                        print("no molID found ")
    return bframe


def get_molecular_profile_by_biomarker_identifier(mid, features=conf_reader.__FEATURES_MOLPROFILES__):
    """
    Retrieves the CIViC molecular profile for a profile ID

    :param mid:
    :param features:
    :return:
    """
    if (mid is not None) and (mid != "") and (mid != " "):
        cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
        cursor = cnx.cursor()

        mysql_q = ("SELECT " + ",".join(
            features) + " from molecular_profiles where molecular_profile='" + mid + "';")
        print(mysql_q)
        cursor.execute(mysql_q)

        res_data = []
        result = cursor.fetchall()

        json_obj = {}
        for ((result)) in enumerate(result):
            json_obj = {}
            for j, feature in enumerate(features):
                json_obj[feature] = result[1][j]
            res_data = copy.deepcopy(json_obj)

        cursor.close()
        cnx.close()
    else:
        res_data=[]

    return res_data


def get_molecular_profile(mid, features=conf_reader.__FEATURES_MOLPROFILES__):
    """
    Retrieves the CIViC molecular profile for a profile ID

    :param mid:
    :param features:
    :return:
    """
    if (mid is not None) and (mid != "") and (mid != " "):
        cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
        cursor = cnx.cursor()

        mysql_q = ("SELECT " + ",".join(
            features) + " from molecular_profiles where molecular_profile_id='" + mid + "';")
        #print(mysql_q)
        cursor.execute(mysql_q)

        res_data = []
        result = cursor.fetchall()

        json_obj = {}
        for ((result)) in enumerate(result):
            json_obj = {}
            for j, feature in enumerate(features):
                json_obj[feature] = result[1][j]
            res_data.append(copy.deepcopy(json_obj))

        cursor.close()
        cnx.close()

    return res_data


def search_for_nearby_mutations(res_data, variants):
    """
    Searches clinical evidence for biomarkers in the same gene of a given mutation

    :param res_data:
    :param variants:
    :return:
    """
    for i, var in enumerate(variants):
        is_var = False
        if ":" in var:
            elements = var.split(":")
            gene = elements[0]
            variant = elements[1]
            variant_str = gene + " " + variant
            is_var = True
            identifier = var
            aaref, pos, aaalt = civic_adapter.tools.parse_genomic_data.parse_variant_exchange(variant)
        else:
            gene = var
            identifier = var

        # Get clinical evidence data
        if (gene is not None) and (aaref is not None) and (pos is not None):
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
            cursor = cnx.cursor()

            mysql_q = ("SELECT " + ",".join(
                conf_reader.__FEATURES__) + " from clinical_evidence where (molecular_profile LIKE '" + gene + "%') " \
                    "and (molecular_profile!='" + gene + " Mutation') and " \
                    " (molecular_profile NOT REGEXP '^" + gene + " " + aaref + pos + "[A-Za-z]+$')" \
                    " and (molecular_profile !='" + gene + " " + aaref + pos + "');")
            print(mysql_q)
            cursor.execute(mysql_q)

            res_data[var] = {}
            res_data[var]["nearby_mutations"] = []
            result = cursor.fetchall()

            features = copy.deepcopy(conf_reader.__FEATURES__)
            json_obj = {}
            for ((result)) in enumerate(result):
                json_obj={}
                for j, feature in enumerate(features):
                    json_obj[feature] = result[1][j]
                res_data[var]["nearby_mutations"].append(copy.deepcopy(json_obj))

            cursor.close()
            cnx.close()

    return res_data


def get_gene_summary(variants,res_data):
    """
    Retrieves gene summaries for all gene names found in a biomarker data set

    :param res_data:
    :param variants:
    :return:
    """
    for i, var in enumerate(variants):
        is_var = False
        if ":" in var:
            elements = var.split(":")
            gene = elements[0]
            variant = elements[1]
            variant_str = gene + " " + variant
            is_var = True
            identifier = var
            aaref, pos, aaalt = civic_adapter.tools.parse_genomic_data.parse_variant_exchange(variant)
        else:
            gene = var
            identifier = var

        # Get clinical evidence data
        if (gene is not None) and (gene!=""):
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__CIVIC_DATABASE__, port=int(conf_reader.__CIVIC_DB_PORT__))
            cursor = cnx.cursor()

            mysql_q = ("SELECT " + ",".join(
                conf_reader.__FEATURES_GENESUM__) + " from gene_summaries where (name='" + gene + "');")
            print(mysql_q)
            cursor.execute(mysql_q)

            res_data[var] = {}
            res_data[var]["gene_summary"] = []
            result = cursor.fetchall()

            features = copy.deepcopy(conf_reader.__FEATURES_GENESUM__)
            json_obj = {}
            for ((result)) in enumerate(result):
                json_obj={}
                for j, feature in enumerate(features):
                    json_obj[feature] = result[1][j]
                res_data[var]["gene_summary"].append(copy.deepcopy(json_obj))

            cursor.close()
            cnx.close()

    return res_data
