import copy
import pymysql
import conf.read_config as conf_reader
from civic_adapter import normalize
import civic_adapter.tools.parse_genomic_data
from civic_adapter.tools.data_structures import merge_dictionaries
import civic_adapter.db_requests


def process_variant_request(q=None,genome_version=None,genomepos=None,pos_as_keys=None):
    """
    Retrieves clinical evidence data for a genetic alteration. Search for the following match types:
        - exact matches: Clinical evidence for the given point mutation
        - same position matches: Clinical evidence for other amino acid exchanges at the same position
        - any mutation at the same position: Clinical evidence for any amino acid exchange at the same position
        - any mutation in gene: Clinical evidence for the gene the genetic alteration is located

    :param q:
    :param genome_version:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    variants = q.split(",")
    res_data = {}

    # molecular profile
    for var in variants:
        elements = var.split(":")
        gene = elements[0]
        variant = elements[1]
        variant_str = gene + " " + variant
        #print("get molecular profile of ",variant_str)
        res_data_new = civic_adapter.db_requests.get_molecular_profile_by_biomarker_identifier(variant_str)
        res_data_new = { var: { "molecular_profile":res_data_new} }
        #print(res_data_new)
        res_data = merge_dictionaries(res_data,res_data_new)
    #print("res data ",res_data)

    # exact matches
    res_data_exact_matches = civic_adapter.db_requests.search_for_exact_variant_matches({}, variants, genomepos, pos_as_keys)
    res_data = merge_dictionaries(res_data, res_data_exact_matches)

    # same position matches
    res_data_same_position = civic_adapter.db_requests.search_for_same_position_matches({}, variants, genomepos, pos_as_keys)
    res_data = merge_dictionaries(res_data, res_data_same_position)

    # any mutation matches at the same position
    res_data_same_position_any_mutation = civic_adapter.db_requests.search_for_same_position_any_mutations({}, variants, genomepos, pos_as_keys)
    res_data = merge_dictionaries(res_data, res_data_same_position_any_mutation)

    # any mutation in gene matches
    res_data_same_gene_any_mutation = civic_adapter.db_requests.search_for_any_mutation_in_same_gene({}, variants)
    res_data = merge_dictionaries(res_data, res_data_same_gene_any_mutation)

    # nearby biomarkers in same gene
    res_data_nearby_mutations = civic_adapter.db_requests.search_for_nearby_mutations({}, variants)
    res_data = merge_dictionaries(res_data, res_data_nearby_mutations)

    # add gene summaries
    res_data_gene_summaries = civic_adapter.db_requests.get_gene_summary(variants, {})
    res_data = merge_dictionaries(res_data, res_data_gene_summaries)

    res_data = set_genome_locations_as_keys(res_data, variants, genomepos, pos_as_keys)

    res_data = normalize.normalize_civic_feature_keys(res_data)
    res_data = normalize.normalize_civic_feature_values(res_data)
    res_data = normalize.map_evidence_levels(res_data)

    #res = {}
    #res["header"] = {}
    #res["header"]["genome_version"] = genome_version
    #res["data"] = res_data

    return res_data


def process_gene_request(q=None,genome_version=None,genomepos=None,pos_as_keys=None):
    """
    Retrieves all clinical evidence data for a whole gene

    :param q:
    :param genome_version:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    variants = q.split(",")
    res_data = {}

    for var in variants:
        variant_str = var
        res_data_new = civic_adapter.db_requests.get_molecular_profile_by_biomarker_identifier(variant_str)
        res_data_new = { var: { "molecular_profile":res_data_new} }
        res_data = merge_dictionaries(res_data,res_data_new)

    # any mutation in gene matches
    res_data_same_gene_any_mutation = civic_adapter.db_requests.search_for_any_mutation_in_same_gene({}, variants)
    res_data = merge_dictionaries(res_data, res_data_same_gene_any_mutation)

    #res_data = set_genome_locations_as_keys(res_data, variants, genomepos, pos_as_keys)
    res_data_new = {}
    for var in res_data:
        res_data_new[var] = {}
        res_data_new[var]["civic"] = res_data[var]
    res_data = res_data_new
    #print("res",res_data)

    res_data = normalize.normalize_civic_feature_keys(res_data)
    res_data = normalize.normalize_civic_feature_values(res_data)
    res_data = normalize.map_evidence_levels(res_data)

    # add gene summaries
    res_data_gene_summaries = civic_adapter.db_requests.get_gene_summary(variants, {})
    res_data = merge_dictionaries(res_data, res_data_gene_summaries)

    #res = {}
    #res["header"] = {}
    #res["header"]["genome_version"] = genome_version
    #res["data"] = res_data

    return res_data


def process_nearby_biomarker_request(q=None,genome_version=None,genomepos=None,pos_as_keys=None):
    """
    Retrieves clinical evidence data for other biomarkers found in the same gene

    :param q:
    :param genome_version:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    variants = q.split(",")
    res_data = {}

    # any mutation in gene matches
    res_data_nearby_mutation = civic_adapter.db_requests.search_for_nearby_mutations({}, variants)
    res_data = merge_dictionaries(res_data, res_data_nearby_mutation)

    res_data = set_genome_locations_as_keys(res_data, variants, genomepos, pos_as_keys)

    res_data = normalize.normalize_civic_feature_keys(res_data)
    res_data = normalize.normalize_civic_feature_values(res_data)
    res_data = normalize.map_evidence_levels(res_data)

    #res = {}
    #res["header"] = {}
    #res["header"]["genome_version"] = genome_version
    #res["data"] = res_data

    return res_data


def set_genome_locations_as_keys(res_data, variants, genomepos, pos_as_keys):
    """
    Switches keys of a biomarker object from "gene name:variant exchange" to genomic locations

    :param res_data:
    :param variants:
    :param genomepos:
    :param pos_as_keys:
    :return:
    """
    if pos_as_keys is not None:
        if pos_as_keys == "genompos":
            res_new = {}
            for i, var in enumerate(variants):
                genompos = genomepos[i]
                res_new[genompos] = {}
                res_new[genompos]["civic"] = res_data[var]
            res_data = res_new
    return res_data
