FROM python:3.11.4-bookworm

RUN apt-get update
RUN apt-get upgrade -y

RUN python3 -m pip install --upgrade pip

RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip

WORKDIR /app/civic-adapter
COPY ./requirements.txt /app/civic-adapter/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/civic-adapter/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 10106

CMD [ "/opt/venv/bin/python3", "/app/civic-adapter/app.py" ]
