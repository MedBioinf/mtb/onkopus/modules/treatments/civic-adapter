import unittest
import civic_adapter.process_request

class TestGetMolProfilesForClinicalEvidence(unittest.TestCase):

    def test_get_molprofiles_for_clinical_evidence(self):
        # get clinical evidence
        gene = "NRAS"
        variant = "Q61L"
        genompos = "chr1:114713908T>A"
        q = gene + ":" + variant
        pos_as_keys = "genompos"
        res_data = civic_adapter.process_request.process_request(q,genomepos=genompos, pos_as_keys=pos_as_keys)
        print(res_data)

        # get mol profiles
        res_data = civic_adapter.process_request.get_molecular_profiles_for_clinical_evidence(res_data)
        print(res_data)

