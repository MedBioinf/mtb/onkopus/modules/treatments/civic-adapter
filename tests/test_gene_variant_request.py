import unittest
import civic_adapter.process_request

class TestGeneVariantRequests(unittest.TestCase):

    def test_gene_variant_request(self):
        gene = "NRAS"
        variant = "Q61L"
        q = gene + ":" + variant

        res_data = civic_adapter.process_request.process_request(q)
        print(res_data)

    def test_gene_variant_request_2(self):
        gene = "TP53"
        variant = "R282W"
        genompos = "chr17:7673776G>A"
        q = gene + ":" + variant
        pos_as_keys = "genompos"

        res_data = civic_adapter.process_request.process_request(q,genomepos=genompos, pos_as_keys=pos_as_keys)
        print(res_data)

