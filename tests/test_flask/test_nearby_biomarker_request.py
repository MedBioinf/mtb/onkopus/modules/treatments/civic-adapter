import unittest, os, io
from app import app


class NearbyBiomarkersTestCase(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_civic_interpretation(self):
        gene = "NRAS"
        variant = "Q61L"
        q = gene + ":" + variant
        response = self.client.get("/civic-adapter/v1/hg38/GenomicNearbyClinicalEvidence?q="+q)
        print("Response ",response.get_data(as_text=True))
