import unittest
from app import app

class TestGeneRequest(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_gene_requests(self):
        q = "KRAS"
        response = self.client.get("/civic-adapter/v1/hg38/GeneClinicalEvidence?q="+q)
        print("Response ",response.get_data(as_text=True))

